from pyparsing import *
from ast_nodes import *

####################
# Wrappers
#################################

class ListContainer:
    def __init__(self, aList):
        self.list = aList


####################
# Parse Actions
#################################

# Statements

def asStmtAssign(parsedString, pos, parsedResults):
    varName = parsedResults[0].varId
    exp = parsedResults[1]
    return StmtAssign(varName, exp)

def asStmtVecAssign(parsedString, pos, parsedResults):
    vecName = parsedResults[0].varId
    indexExp = parsedResults[1]
    exp = parsedResults[2]
    return StmtVecAssign(vecName, indexExp, exp)

def asStmtIfElse(parsedString, pos, parsedResults):
    boolExp = parsedResults[0]
    thenBlock = parsedResults[1]
    elseBlock = parsedResults[2]
    return StmtIfElse(boolExp, thenBlock, elseBlock)

def asStmtIf(parsedString, pos, parsedResults):
    boolExp = parsedResults[0]
    block = parsedResults[1]
    return StmtIf(boolExp, block)

def asStmtWhile(parsedString, pos, parsedResults):
    boolExp = parsedResults[0]
    block = parsedResults[1]
    return StmtWhile(boolExp, block)

def asStmtReturn(parsedString, pos, parsedResults):
    exp = parsedResults[0]
    return StmtReturn(exp)

def asStmtCall(parsedString, pos, parsedResults):
    funName = parsedResults[0].varId
    params = parsedResults[1].list
    return StmtCall(funName, params)


# Expressions

def asNum(parsedString, pos, parsedResults):
    num = parsedResults[0]
    return ExprConstNum(num)

def asTrue(parsedString, pos, parsedResults):
    return ExprConstBool(True)

def asFalse(parsedString, pos, parsedResults):
    return ExprConstBool(False)

def asExprVecLength(parsedString, pos, parsedResults):
    vecId = parsedResults[0].varId
    return ExprVecLength(vecId)

def asExprVecMake(parsedString, pos, parsedResults):
    exps = parsedResults[0].list
    return ExprVecMake(exps)

def asExprVecDeref(parsedString, pos, parsedResults):
    name = parsedResults[0].varId
    exp = parsedResults[1]
    return ExprVecDeref(name, exp)

def asExprCall(parsedString, pos, parsedResults):
    name = parsedResults[0].varId
    exps = parsedResults[1].list
    return ExprCall(name, exps)

def asExprVar(parsedString, pos, parsedResults):
    name = parsedResults[0]
    return ExprVar(name)

def operatorsExp(parsedResults, classes):
    results = [result for result in parsedResults]
    leftExp = results.pop(0)
    while results:
        operator = results.pop(0)
        rightExp = results.pop(0)
        leftExp = classes[operator](leftExp, rightExp)
    return leftExp

def asMulExpression(parsedString, pos, parsedResults):
    return operatorsExp(parsedResults, {'*': ExprMul})

def asAddExpression(parsedString, pos, parsedResults):
    return operatorsExp(parsedResults, {'+': ExprAdd, '-': ExprSub})

def asExprLe(parsedString, pos, parsedResults):
    return ExprLe(parsedResults[0], parsedResults[1])

def asExprGe(parsedString, pos, parsedResults):
    return ExprGe(parsedResults[0], parsedResults[1])

def asExprLt(parsedString, pos, parsedResults):
    return ExprLt(parsedResults[0], parsedResults[1])

def asExprGt(parsedString, pos, parsedResults):
    return ExprGt(parsedResults[0], parsedResults[1])

def asExprEq(parsedString, pos, parsedResults):
    return ExprEq(parsedResults[0], parsedResults[1])

def asExprNe(parsedString, pos, parsedResults):
    return ExprNe(parsedResults[0], parsedResults[1])

def asNegatedExpression(parsedString, pos, parsedResults):
    results = [result for result in parsedResults]
    exp = results.pop()
    if len(results) % 2 == 0:
        return exp
    else:
        return ExprNot(exp)

def asLogicExpression(parsedString, pos, parsedResults):
    return operatorsExp(parsedResults, {'and': ExprAnd, 'or': ExprOr})


# Parents

def asBlock(parsedString, pos, parsedResults):
    #print 'asBlock'
    instructions = [instruction for instruction in parsedResults]
    return Block(instructions)

def asParameter(parsedString, pos, parsedResults):
    #print 'asParameter'
    name = parsedResults[0].varId
    pType = parsedResults[1]
    return Parameter(name, pType)

def asProgram(parsedString, pos, parsedResults):
#    print 'asProgram'
    return Program([fun for fun in parsedResults])

def asFunction(parsedString, pos, parsedResults):
#    print '<<< asFunction'
    name = parsedResults[0].varId
    params = parsedResults[1].list
    if len(parsedResults) == 4:
        rType = parsedResults[2]
        block = parsedResults[3]
    else:
        rType = Unit()
        block = parsedResults[2]
    return Function(name, params, rType, block)


# Other

def asList(parsedString, pos, parsedResults):
#   print '<<< asList'
    elems = [elem for elem in parsedResults]
    return ListContainer(elems)

####################
# Rules
#################################

# Symbols
LPAREN = Literal('(').suppress()
RPAREN = Literal(')').suppress()
COMMA = Literal(',').suppress()
LBRACK = Literal('[').suppress()
RBRACK = Literal(']').suppress()
LBRACE = Literal('{').suppress()
RBRACE = Literal('}').suppress()
ASSIGN = Literal(':=').suppress()
COLON = Literal(':').suppress()
HASH = Literal('#').suppress()
LE = Literal('<=').suppress()
GE = Literal('>=').suppress()
LT = Literal('<').suppress()
GT = Literal('>').suppress()
EQ = Literal('==').suppress()
NE = Literal('!=').suppress()
PLUS = Literal('+')
MINUS = Literal('-')
TIMES = Literal('*')

# Keywords
BOOL = Literal('Bool')
INT = Literal('Int')
VEC = Literal('Vec')
TRUE = Literal('True')
FALSE = Literal('False')
AND = Literal('and')
ELSE = Literal('else').suppress()
FUN = Literal('fun').suppress()
IF = Literal('if').suppress()
NOT = Literal('not')
OR = Literal('or')
RETURN = Literal('return').suppress()
WHILE = Literal('while').suppress()

# Rules

TYPE = BOOL | INT | VEC
FUN_TYPE = BOOL | INT
ID = Word(alphas+"_", alphanums+"_")
NUM = Word(nums)

EXPRESSION = Forward()

EXPRESSION_LIST = Forward()

ATOMIC_EXPRESSION = NUM.setParseAction(asNum) \
                    | TRUE.setParseAction(asTrue) \
                    | FALSE.setParseAction(asFalse) \
                    | (HASH + ID).setParseAction(asExprVecLength) \
                    | LPAREN + EXPRESSION + RPAREN \
                    | (LBRACK + EXPRESSION_LIST + RBRACK).setParseAction(asExprVecMake) \
                    | (ID + LBRACK + EXPRESSION + RBRACK).setParseAction(asExprVecDeref) \
                    | (ID + LPAREN + EXPRESSION_LIST + RPAREN).setParseAction(asExprCall) \
                    | (ID).setParseAction(asExprVar)

MULTIPLICATIVE_EXPRESSION = (ATOMIC_EXPRESSION + ZeroOrMore(TIMES + ATOMIC_EXPRESSION)).setParseAction(asMulExpression)

ADDITIVE_EXPRESSION =   (MULTIPLICATIVE_EXPRESSION \
                        + ZeroOrMore((PLUS | MINUS) + MULTIPLICATIVE_EXPRESSION)).setParseAction(asAddExpression)

RATIONAL_EXPRESSION =   (ADDITIVE_EXPRESSION + LE + ADDITIVE_EXPRESSION).setParseAction(asExprLe) \
                        | (ADDITIVE_EXPRESSION + GE + ADDITIVE_EXPRESSION).setParseAction(asExprGe) \
                        | (ADDITIVE_EXPRESSION + LT + ADDITIVE_EXPRESSION).setParseAction(asExprLt) \
                        | (ADDITIVE_EXPRESSION + GT + ADDITIVE_EXPRESSION).setParseAction(asExprGt) \
                        | (ADDITIVE_EXPRESSION + EQ + ADDITIVE_EXPRESSION).setParseAction(asExprEq) \
                        | (ADDITIVE_EXPRESSION + NE + ADDITIVE_EXPRESSION).setParseAction(asExprNe) \
                        | ADDITIVE_EXPRESSION

ATOMIC_LOGIC_EXPRESSION = (ZeroOrMore(NOT) + RATIONAL_EXPRESSION).setParseAction(asNegatedExpression)

LOGIC_EXPRESSION =  (ATOMIC_LOGIC_EXPRESSION \
                    + ZeroOrMore((AND | OR) + ATOMIC_LOGIC_EXPRESSION)).setParseAction(asLogicExpression)

EXPRESSION << LOGIC_EXPRESSION

EXPRESSION_LIST << Optional(delimitedList(EXPRESSION)).setParseAction(asList)

BLOCK = Forward()

INSTRUCTION =   (ID + ASSIGN + EXPRESSION).setParseAction(asStmtAssign) \
                | (ID + LBRACK + EXPRESSION + RBRACK + ASSIGN + EXPRESSION).setParseAction(asStmtVecAssign) \
                | (IF + EXPRESSION + BLOCK + ELSE + BLOCK).setParseAction(asStmtIfElse) \
                | (IF + EXPRESSION + BLOCK).setParseAction(asStmtIf) \
                | (WHILE + EXPRESSION + BLOCK).setParseAction(asStmtWhile) \
                | (RETURN + EXPRESSION).setParseAction(asStmtReturn) \
                | (ID + LPAREN + EXPRESSION_LIST + RPAREN).setParseAction(asStmtCall)

BLOCK << (LBRACE + ZeroOrMore(INSTRUCTION) + RBRACE).setParseAction(asBlock)

PARAM = (ID + COLON + TYPE).setParseAction(asParameter)
PARAMS = (LPAREN.suppress() + Optional(delimitedList(PARAM)) + RPAREN.suppress()).setParseAction(asList)

FUN_DECL = (FUN + ID + PARAMS + Optional(COLON + FUN_TYPE) + BLOCK).setParseAction(asFunction)

PROGRAM = ZeroOrMore(FUN_DECL).setParseAction(asProgram)