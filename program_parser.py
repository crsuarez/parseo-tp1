import re

class Parser:
    def __init__(self, rules):
        self.rules = rules

    def removeComments(self, fileText):
        return re.sub(r"//.*?\n", "\n", fileText)

    def parse(self, filename):
        with open(filename) as program:
            fileText = self.removeComments(program.read())
            result = self.rules.parseString(fileText)[0]
            return result

    def semanticAnalysis(self, AST):
        return AST.analyze()
