NATIVE_FUNCTIONS = ['putChar', 'putNum']


class AST_Node:
    def __init__(self):
        self.indent = '  '

    def __repr__(self):
        return str(self)

class Program(AST_Node):
    def __init__(self, functions):
        AST_Node.__init__(self)
        self.functions = {}
        for fun in functions:
            if fun.name in self.functions:
                raise Exception('Function ' + fun.name + ' is defined more than once')
            else:
                self.functions[fun.name] = fun

    def __str__(self):
        text = '(Program'
        for fun in self.functions.values():
            text += '\n' + str(fun)
        text += '\n)'
        return text

    def analyze(self):
        for fun in self.functions.values():
            errorMap = fun.analyze(self)
            if errorMap:
                return errorMap
        return {}


class Function(AST_Node):
    def __init__(self, name, params, rType, block):
        AST_Node.__init__(self)
        self.name = name
        self.type = rType
        self.block = block

        self.params = {}
        for param in params:
            if param.name in self.params:
                raise Exception('Parameter ' + param.name + ' is defined more than once')
            else:
                self.params[param.name] = param

    def paramsToStr(self):
        text = ''
        if self.params:
            for param in self.params.values():
                text += str(param)
            text += '\n'
        return text

    def __str__(self):
        return '(Function\n' + self.name + '\n' + str(self.type) + '\n' + self.paramsToStr() + str(self.block) + ')'

    def analyze(self,program):
        return self.block.analyze(program,self)

## Block

class Block(AST_Node):
    def __init__(self, instructions):
        AST_Node.__init__(self)
        self.instructions = instructions

    def __str__(self):
        text = '(Block'
        for inst in self.instructions:
            text += '\n' + str(inst)
        text += '\n)'
        return text

    def analyze(self,program,fun,context = None):
        if context:
            self.context = context
            for instr in self.instructions:
                if instr.__class__.__name__ == StmtReturn.__name__:
                    return {'invalidReturnStmtError' : 'The return statement must be called at the end of a function'}
        else:
            self.context = {name : param.type for name, param in fun.params.items()}
            lastPos = len(self.instructions) - 1
            i = 0
            for instr in self.instructions:
                if instr.__class__.__name__ == StmtReturn.__name__ and lastPos != self.instructions.index(instr):
                    return {'invalidReturnStmtError' : 'The return statement must be called at the end of a function'}
                i += 1

        for instr in self.instructions:
            errorsMap = instr.analyze(program, self.context, fun)
            if errorsMap:
                return errorsMap
            else:
                instr.addToContext(program, self.context)
        return {}

## Expresiones

class ExprInt(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def getType(self,program,context):
        return Int()

class ExprBool(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def getType(self,program,context):
        return Bool()

class ExprVec(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def getType(self,program,context):
        return Vec()

class ExprVar(AST_Node):
    def __init__(self, varId):
        AST_Node.__init__(self)
        self.varId = varId

    def __str__(self):
        return '(ExprVar\n' + self.varId + '\n)'

    def getType(self,program,context):
        return context[self.varId]

    def analyze(self,program,context):
        if self.varId in context:
            return {}
        else:
            return {'exprVarError' : 'The variable "' + self.varId + '" was not found in current context'}


class ExprConstNum(ExprInt):
    def __init__(self, constNum):
        ExprInt.__init__(self)
        self.constNum = constNum

    def __str__(self):
        return '(ExprConstNum\n' + self.constNum + '\n)'

    def analyze(self,program,context):
        return {}

class ExprConstBool(ExprBool):
    def __init__(self, constBool):
        ExprBool.__init__(self)
        self.constBool = constBool

    def __str__(self):
        return '(ExprConstBool\n' + str(self.constBool) + '\n)'    

    def analyze(self,program,context):
        return {}    

class ExprVecMake(ExprVec):
    def __init__(self, numExpressions):
        ExprVec.__init__(self)
        self.numExpressions = numExpressions

    def __str__(self):
        text = '(ExprVecMake'
        for numExp in self.numExpressions:
            text += '\n' + str(numExp)
        text += '\n)'
        return text

    def analyze(self, program, context):
        for numExp in self.numExpressions:
            errorsMap = numExp.analyze(program, context)
            if errorsMap:
                return errorsMap
            else:
                expType = numExp.getType(program, context)
                if expType != Int():
                    return {'exprVecItemError' : 'A Vec can only contain ' + str(Int()) + ' items and not ' + str(expType)}
        return {} 

class ExprVecLength(ExprInt):
    def __init__(self, vecId):
        self.vecId = vecId

    def __str__(self):
        return '(ExpreVecLength\n' + self.vecId + '\n)'

    def analyze(self,program,context):
        if self.vecId in context:
            return {}
        else:
            return {'exprVecLengthError' : 'The VecId ' + self.vecId + ' was not found in current context'}


class ExprVecDeref(ExprInt):
    def __init__(self, vecId, numExp):
        ExprInt.__init__(self)
        self.vecId = vecId
        self.numExp = numExp

    def __str__(self):
        return '(ExprVecDeref\n' + self.vecId + '\n' + str(self.numExp) + '\n)'

    def analyze(self,program,context):
        errorsMap = self.numExp.analyze(program,context)
        if errorsMap:
            return errorsMap
        else:
            indexType = str(self.numExp.getType(program,context))
            if indexType != Int.__name__:
                    return {'invalidIndexExpressionType':'The index type received is ' + str(indexType) + ' but ' + Int.__name__ + ' was expected'}
            else:
                if self.vecId in context:
                    return {}
                else:
                    return {'exprVecDerefError' : 'The vecId ' + self.vecId + ' was not found in current context'}

class ExprCall(AST_Node):
    def __init__(self, funId, params):
        AST_Node.__init__(self)
        self.funId = funId
        self.params = params

    def __str__(self):
        text = '(ExprCall'
        text += '\n' + self.funId
        for param in self.params:
            text += '\n' + str(param)
        text += '\n)'
        return text

    def getType(self, program, context):
        return program.functions[self.funId].type

    def analyze(self, program, context):
        for param in self.params:
            errorsMap = param.analyze(program, context)
            if errorsMap:
                return errorsMap

        if self.funId in program.functions:
            calledFun = program.functions[self.funId]
            if len(calledFun.params) != len(self.params):
                return {'paramsLengthError' : 'Expected "' + len(calledFun.params) \
                    + '"" params in function "' + calledFun.name + '" but got "' + len(self.params) + '"'}
            else:
                i = 0
                for funParam in calledFun.params.values():
                    funParamType = funParam.type
                    callParamType = str(self.params[i].getType(program, context))
                    if funParamType != callParamType:
                        return {'paramTypeError' : 'Wrong param type in param number "' + str(i) + '" in function "' + fun.name + '", "' + callParamType + '" received but "' + funParamType + '" expected'}
                    i += 1
                return {}
        else:
            if self.funId not in NATIVE_FUNCTIONS:
                return {'functionCallError' : 'The function "' + self.funId + '" is not defined'}
            else:
                return {}

class ExprAnd(ExprBool):
    def __init__(self, boolExp1, boolExp2):
        ExprBool.__init__(self)
        self.boolExp1 = boolExp1
        self.boolExp2 = boolExp2

    def __str__(self):
        return '(ExprAnd\n' + str(self.boolExp1) + '\n' + str(self.boolExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.boolExp1.analyze(program,context)
        errorsMap2 = self.boolExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.boolExp1.getType(program,context))
            expr2Type = str(self.boolExp2.getType(program,context))
            if expr1Type == expr2Type == Bool.__name__:
                return {}
            else:
                return {'exprAddError':'Cannot conjunt ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprOr(ExprBool):
    def __init__(self, boolExp1, boolExp2):
        ExprBool.__init__(self)
        self.boolExp1 = boolExp1
        self.boolExp2 = boolExp2

    def __str__(self):
        return '(ExprOr\n' + str(self.boolExp1) + '\n' + str(self.boolExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.boolExp1.analyze(program,context)
        errorsMap2 = self.boolExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.boolExp1.getType(program,context))
            expr2Type = str(self.boolExp2.getType(program,context))
            if expr1Type == expr2Type == Bool.__name__:
                return {}
            else:
                return {'exprOrError':'Cannot disjunt ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprNot(ExprBool):
    def __init__(self, boolExp):
        ExprBool.__init__(self)
        self.boolExp = boolExp

    def __str__(self):
        return '(ExprNot\n' + str(self.boolExp) + '\n)'

    def analyze(self,program,context):
        errorsMap = self.boolExp.analyze(program,context)
        if errorsMap:
            return errorsMap
        else: 
            exprType = str(self.boolExp.getType(program,context))
            if exprType == Bool.__name__:
                return {}
            else:
                return {'exprNotError':'Cannot negate an expression of type' + str(expr1Type)}

class ExprLe(ExprBool):
    def __init__(self, numExp1, numExp2):
        ExprBool.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def __str__(self):
        return '(ExprLe\n' + str(self.numExp1) + '\n' + str(self.numExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.numExp1.analyze(program,context)
        errorsMap2 = self.numExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.numExp1.getType(program,context))
            expr2Type = str(self.numExp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprLeError':'Cannot compare ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprGe(ExprBool):
    def __init__(self, numExp1, numExp2):
        ExprBool.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def __str__(self):
        return '(ExprGe\n' + str(self.numExp1) + '\n' + str(self.numExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.numExp1.analyze(program,context)
        errorsMap2 = self.numExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.numExp1.getType(program,context))
            expr2Type = str(self.numExp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprGeError':'Cannot compare ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprLt(ExprBool):
    def __init__(self, numExp1, numExp2):
        ExprBool.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def __str__(self):
        return '(ExprLt\n' + str(self.numExp1) + '\n' + str(self.numExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.numExp1.analyze(program,context)
        errorsMap2 = self.numExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.numExp1.getType(program,context))
            expr2Type = str(self.numExp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprLtError':'Cannot compare ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprGt(ExprBool):
    def __init__(self, numExp1, numExp2):
        ExprBool.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def __str__(self):
        return '(ExprGt\n' + str(self.numExp1) + '\n' + str(self.numExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.numExp1.analyze(program,context)
        errorsMap2 = self.numExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.numExp1.getType(program,context))
            expr2Type = str(self.numExp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprGtError':'Cannot compare ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprEq(ExprBool):
    def __init__(self, exp1, exp2):
        ExprBool.__init__(self)
        self.exp1 = exp1
        self.exp2 = exp2

    def __str__(self):
        return '(ExprEq\n' + str(self.exp1) + '\n' + str(self.exp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.exp1.analyze(program,context)
        errorsMap2 = self.exp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.exp1.getType(program,context))
            expr2Type = str(self.exp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprEqError':'Cannot compare ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprNe(ExprBool):
    def __init__(self, exp1, exp2):
        ExprBool.__init__(self)
        self.exp1 = exp1
        self.exp2 = exp2

    def __str__(self):
        return '(ExprNe\n' + str(self.exp1) + '\n' + str(self.exp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.exp1.analyze(program,context)
        errorsMap2 = self.exp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.exp1.getType(program,context))
            expr2Type = str(self.exp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprNeError':'Cannot compare ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprAdd(ExprInt):
    def __init__(self, numExp1, numExp2):
        ExprInt.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def __str__(self):
        return '(ExprAdd\n' + str(self.numExp1) + '\n' + str(self.numExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.numExp1.analyze(program,context)
        errorsMap2 = self.numExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.numExp1.getType(program,context))
            expr2Type = str(self.numExp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprAddError':'Cannot add ' + str(expr1Type) + ' and ' + str(expr2Type)}


class ExprSub(ExprInt):
    def __init__(self, numExp1, numExp2):
        ExprInt.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def __str__(self):
        return '(ExprSub\n' + str(self.numExp1) + '\n' + str(self.numExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.numExp1.analyze(program,context)
        errorsMap2 = self.numExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.numExp1.getType(program,context))
            expr2Type = str(self.numExp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprSubError':'Cannot sub ' + str(expr1Type) + ' and ' + str(expr2Type)}

class ExprMul(ExprInt):
    def __init__(self, numExp1, numExp2):
        ExprInt.__init__(self)
        self.numExp1 = numExp1
        self.numExp2 = numExp2

    def __str__(self):
        return '(ExprMul\n' + str(self.numExp1) + '\n' + str(self.numExp2) + '\n)'

    def analyze(self,program,context):
        errorsMap1 = self.numExp1.analyze(program,context)
        errorsMap2 = self.numExp2.analyze(program,context)
        if errorsMap1:
            return errorsMap1
        elif errorsMap2:
            return errorsMap2
        else: 
            expr1Type = str(self.numExp1.getType(program,context))
            expr2Type = str(self.numExp2.getType(program,context))
            if expr1Type == expr2Type == Int.__name__:
                return {}
            else:
                return {'exprMulError':'Cannot multiple "' + str(expr1Type) + '" and "' + str(expr2Type) + '"'}

## Statements

class Stmt(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def addToContext(self,context,program):
        return context


class StmtAssign(Stmt):
    def __init__(self, varId, exp):
        Stmt.__init__(self)
        self.varId = varId
        self.exp = exp

    def __str__(self):
        return '(StmtAssign\n' + self.varId + '\n' + str(self.exp) + '\n)'

    def addToContext(self, program, context):
        context[self.varId] = self.exp.getType(program,context)

    def analyze(self, program, context, fun):
        #Validar expresion primero
        expErrorsMap = self.exp.analyze(program,context)
        if expErrorsMap:
            return expErrorsMap
        else:
            if self.varId in context:
                anotherType = str(self.exp.getType(program,context))
                #print anotherType.__class__, context[self.varId].__class__
                if anotherType != str(context[self.varId]):
                    return {'invalidVarTypeAssign':'Tried to assign ' + str(anotherType) + ' to ' + self.varId + ' but ' + str(context[self.varId]) + ' was expected'}
            #else:
            #        return {'varNameNotFound' : 'The varId "' + self.varId + '" is not declared in current context'}
            return {}


class StmtVecAssign(Stmt):
    def __init__(self, varId, indexExp, exp):
        Stmt.__init__(self)
        self.varId = varId
        self.indexExp = indexExp
        self.exp = exp

    def __str__(self):
        return '(StmtVecAssign\n' + self.varId + '\n' + str(self.indexExp) + '\n' + str(self.exp) + '\n)'

    def analyze(self, program, context, fun):
        #Analizar expresion del indice
        indexExpErrorsMap = self.indexExp.analyze(program,context)
        if indexExpErrorsMap:
            return indexExpErrorsMap
        else:
            #Analizar expresion a asignar
            expErrorsMap = self.exp.analyze(program,context)
            if expErrorsMap:
                return expErrorsMap
            else:
                if self.varId in context:
                    indexType = str(self.indexExp.getType(program,context))
                    if indexType != Int.__name__:
                        return {'invalidIndexExpressionType':'The index type received is ' + str(indexType) + ' but ' + Int.__name__ + ' was expected'}
                    expType = str(self.exp.getType(program,context))
                    if expType != Int.__name__:
                        return {'invalidExpressionToAssingType':'The expression type received is ' + str(indexType) + ' but ' + Int.__name__ + ' was expected'}
                else:
                    return {'varNameNotFound' : 'The varId ' + self.varId + ' is not declared in current context'}
                return {}

       


class StmtIf(Stmt):
    def __init__(self, boolExp, block):
        Stmt.__init__(self)
        self.boolExp = boolExp
        self.block = block

    def __str__(self):
        return '(StmtIf\n' + str(self.boolExp) + '\n' + str(self.block) + '\n)'

    def analyze(self, program, context, fun):
        errorsExpMap = self.boolExp.analyze(program,context)
        errorsBlockMap = self.block.analyze(program, fun, context)
        if errorsExpMap:
            return errorsExpMap
        elif errorsBlockMap:
            return errorsBlockMap
        else: 
            exprType = self.boolExp.getType(program,context)
            if  exprType == Bool():
                return {}
            else:
                return {'stmtIfError':'The condition expression type is' + str(exprType) +  ' but ' +  str(Bool()) + ' was expected'}

class StmtIfElse(Stmt):
    def __init__(self, boolExp, thenBlock, elseBlock):
        Stmt.__init__(self)
        self.boolExp = boolExp
        self.thenBlock = thenBlock
        self.elseBlock = elseBlock

    def __str__(self):
        return '(StmtIfElse\n' + str(self.boolExp) + '\n' + str(self.thenBlock) + '\n' + str(self.elseBlock) + '\n)'

    def analyze(self, program, context, fun):
        errorsExpMap = self.boolExp.analyze(program, context)
        errorsThenBlockMap = self.thenBlock.analyze(program, fun, context)
        errorsElseBlockMap = self.elseBlock.analyze(program, fun, context)
        if errorsExpMap:
            return errorsExpMap
        elif errorsThenBlockMap:
            return errorsThenBlockMap
        elif errorsElseBlockMap:
            return errorsElseBlockMap
        else: 
            exprType = self.boolExp.getType(program,context)
            if  exprType == Bool():
                return {}
            else:
                return {'stmtIfElseError':'The condition expression type is' + str(exprType) +  ' but ' +  str(Bool()) + ' was expected'}


class StmtWhile(Stmt):
    def __init__(self, boolExp, block):
        Stmt.__init__(self)
        self.boolExp = boolExp
        self.block = block

    def __str__(self):
        return '(StmtWhile\n' + str(self.boolExp) + '\n' + str(self.block) + '\n)'

    def analyze(self, program, context, fun):
        errorsExpMap = self.boolExp.analyze(program, context)
        errorsBlockMap = self.block.analyze(program, fun, context)
        if errorsExpMap:
            return errorsExpMap
        elif errorsBlockMap:
            return errorsBlockMap
        else: 
            exprType = self.boolExp.getType(program, context)
            if  exprType == Bool():
                return {}
            else:
                return {'stmtWhileError':'The condition expression type is' + str(exprType) +  ' but ' +  str(Bool()) + ' was expected'}

class StmtReturn(Stmt):
    def __init__(self, exp):
        Stmt.__init__(self)
        self.exp = exp

    def __str__(self):
        return '(StmtReturn\n' + str(self.exp) + '\n)'

    def analyze(self, program, context, fun):
        errorsMap = self.exp.analyze(program, context)
        if errorsMap:
            return errorsMap
        else:
            retType = str(self.exp.getType(program, context))
            funRetType = program.functions[fun.name].type
            if retType != funRetType:
                return {'stmtReturnError' : 'Wrong return value type, returned "' + str(retType) + '" but "' + str(funRetType) + '" was expected'}
            elif retType == Vec.__name__:
                return {'stmtReturnVecError' : 'A function cannot return a "Vec" value'}
            return {}

class StmtCall(Stmt):
    def __init__(self, funId, params):
        Stmt.__init__(self)
        self.funId = funId
        self.params = params

    def __str__(self):
        text = '(StmtCall'
        text += '\n' + self.funId
        for param in self.params:
            text += '\n' + str(param)
        text += '\n)'
        return text

    def analyze(self, program, context, fun):
        for param in self.params:
            errorsMap = param.analyze(program, context)
            if errorsMap:
                return errorsMap

        if self.funId in program.functions:
            calledFun = program.functions[self.funId]
            if len(calledFun.params) != len(self.params):
                return {'paramsLengthError' : 'Expected "' + len(calledFun.params) \
                    + '"" params in function "' + calledFun.name + '" but got "' + len(self.params) + '"'}
            else:
                i = 0
                for funParam in calledFun.params.values():
                    funParamType = funParam.type
                    callParamType = str(self.params[i].getType(program, context))
                    if funParamType != callParamType:
                        return {'paramTypeError' : 'Wrong param type in param number "' + str(i) + '" in function "' + fun.name + '", "' + callParamType + '" received but "' + funParamType + '" expected'}
                    i += 1
                return {}
        else:
            if self.funId not in NATIVE_FUNCTIONS:
                return {'functionCallError' : 'The function "' + self.funId + '" is not defined'}
            else:
                return {}

## Parameters

class Parameter(AST_Node):
    def __init__(self, name, pType):
        AST_Node.__init__(self)
        self.name = name
        self.type = pType

    def __str__(self):
        return '(Parameter\n' + str(self.name) + '\n' + str(self.type) + '\n)'

## Types

class Type(AST_Node):
    def __init__(self):
        AST_Node.__init__(self)

    def __eq__(self,anotherType):
        return self.__class__.__name__ == anotherType.__class__.__name__

    def __ne__(self,anotherType):
        return self.__class__.__name__ != anotherType.__class__.__name__

    def __str__(self):
        return self.__class__.__name__

class Int(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Int'

class Bool(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Bool'

class Unit(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Unit'

class Vec(Type):
    def __init__(self):
        Type.__init__(self)

    def __str__(self):
        return 'Vec'

