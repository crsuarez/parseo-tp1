from program_parser import Parser
from rules import PROGRAM as rules

if __name__ == '__main__':
	parser = Parser(rules)
	for n in range(0,11):
		nString = str(n)
		num = '0' + nString if n < 10 else nString
		AST = parser.parse('examples/test' + num + '.input')
		#print AST
		print ('Test ' + num + ': ' + str(AST.analyze()))